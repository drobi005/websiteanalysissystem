package edu.odu.cs.cs350;

import static org.junit.Assert.*;



import org.junit.Before;

import static org.hamcrest.CoreMatchers.containsString;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.StringReader;
import java.io.File;
import java.io.IOException;
import java.util.Scanner; 

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 1 - Does this piece of code perform the operations
 *     it was designed to perform?
 *
 * 2 - Does this piece of code do something it was not
 *     designed to perform?
 *
 * 1 Test per mutator
 */

public class TestHTMLExtractor {
	String noHTML;
	String hasHTML;
	String HTML404;
	String testTagsHTML;
	String HTMLMultipleLinks;
	HTMLExtractor myExtractorNo;
	HTMLExtractor myExtractorHas;
	HTMLExtractor myExtractor404;
	HTMLExtractor myExtractorMultipleLinks;
	HTMLExtractor myExtractorTags;
	
	@Before
	public void setUp() throws Exception {
		noHTML = "noHTML.txt";
		hasHTML = "hasHTML.php";
		HTML404 = "404.html";
		HTMLMultipleLinks = "";
		testTagsHTML = "testTagsHTML.html";
		myExtractorNo = new HTMLExtractor();
		myExtractorHas = new HTMLExtractor();
		myExtractor404 = new HTMLExtractor();
		myExtractorMultipleLinks = new HTMLExtractor();
		myExtractorTags = new HTMLExtractor();
	}

	@Test
	public void testHTMLExtractor() {
		myExtractorNo.setFilePath(noHTML);
		assertThat(myExtractorNo.getFilePath(), containsString(noHTML));
		
		myExtractorHas.setFilePath(hasHTML);
		assertThat(myExtractorHas.getFilePath(), containsString(hasHTML));
	}

	@Test
	public void testSetFilePath() {
		myExtractorNo.setFilePath("Hello");
		assertThat(myExtractorNo.getFilePath(), containsString("Hello"));
		myExtractorNo.setFilePath("World");
		assertThat(myExtractorNo.getFilePath(), containsString("World"));
	}

	@Test
	public void testLookForHTML() throws IOException {
		myExtractorNo.setFilePath(noHTML);
		myExtractorNo.lookForHTML();
		assertThat(myExtractorNo.getWillBeAnalyzed(), is(false));

		myExtractorHas.setFilePath(hasHTML);
		assertThat(myExtractorHas.getFilePath(), containsString(hasHTML));
		myExtractorHas.lookForHTML();
		assertThat(myExtractorHas.getWillBeAnalyzed(), is(true));
	}
	
	@Test
	public void testAnalyzeTheHTML() throws IOException
	{
		// Test proper tags are extracted
		myExtractorTags.setFilePath("testTagsHTML.html");
		myExtractorTags.lookForHTML();
		assertThat(myExtractorTags.getNumImages(), is(2));
		assertThat(myExtractorTags.getNumStylesheets(), is(2));
		assertThat(myExtractorTags.getNumScripts(), is(2));
		assertThat(myExtractorTags.getNumLinks(), is(4));
		assertThat(myExtractorTags.getNumVideo(), is(1));
		assertThat(myExtractorTags.getNumAudio(), is(1));
		assertThat(myExtractorTags.getNumOther(), is(1));
		
		// No tags should be extracted
		myExtractorHas.setFilePath("hasHTML.php");
		myExtractorHas.lookForHTML();
		assertThat(myExtractorHas.getNumImages(), is(0));
		assertThat(myExtractorHas.getNumStylesheets(), is(0));
		assertThat(myExtractorHas.getNumScripts(), is(0));
		assertThat(myExtractorHas.getNumLinks(), is(0));
		assertThat(myExtractorHas.getNumVideo(), is(0));
		assertThat(myExtractorHas.getNumAudio(), is(0));
		//assertThat(myExtractorHas.getNumUncategorized(), is(0));
	}
	
	@Test
	public void testClassifyImage() throws IOException
	{
		myExtractorTags.setFilePath("testTagsHTML.html");
		myExtractorTags.lookForHTML();
		assertThat(myExtractorTags.getInternalImages(), is(1));
		assertThat(myExtractorTags.getExternalImages(), is(1));
	}
	
	@Test
	public void testClassifyLink() throws IOException
	{
		myExtractorTags.setFilePath("testTagsHTML.html");
		myExtractorTags.lookForHTML();
		assertThat(myExtractorTags.getIntraPageLinks(), is(2));
		assertThat(myExtractorTags.getIntraSiteLinks(), is(1));
		assertThat(myExtractorTags.getExternalLinks(), is(1));
	}

	@Test
	public void testNotError404Checker() throws IOException {
		myExtractor404.setFilePath(HTML404);
		myExtractor404.lookForHTML();
		assertThat(myExtractor404.getWillBeAnalyzed(),is(false));
	}
	
	@Test
	public void testTranslateLinks() throws IOException{
		myExtractorMultipleLinks.setFilePath(HTMLMultipleLinks);
		//myExtractorMultipleLinks.lookForHTML();
		myExtractorMultipleLinks.setRootPath("/home/user/website/");
		
		List<String> original = new ArrayList<String>();
		original.add("/home/user/website/Public/index.html");
		original.add("/home/user/website/Public/scripts/script.js");
		original.add("../Public/style/sitestyle.css");
		
		List<String> translated = new ArrayList<String>();
		translated.add("Public/index.html");
		translated.add("Public/scripts/script.js");
		translated.add("Public/style/sitestyle.css");
		
		File file1 = new File (original.get(0));
		File file2 = new File (original.get(1));
		File file3 = new File (original.get(2));
		
		assertEquals(translated.get(0),myExtractorMultipleLinks.translateLinks(file1));
		assertEquals(translated.get(1),myExtractorMultipleLinks.translateLinks(file2));
		assertEquals(translated.get(2),myExtractorMultipleLinks.translateLinks(file3));
		
		
	}

	/*@Test
	public void testGetFileSize()
	{
		HTMLExtractor data = new HTMLExtractor();
		data.setFilePath("");
		data.lookForHTML();
		data.getFileSize();
		
		assertTrue(fileSize > 0);
		assertEquals(magnitude, 1);
	}*/

}
