package edu.odu.cs.cs350;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;

/**
 * 1 - Does this piece of code perform the operations
 *     it was designed to perform?
 *
 * 2 - Does this piece of code do something it was not
 *     designed to perform?
 *
 * 1 Test per mutator 
 */

public class TestCLI {
	
	CLI defaultConTest,SBC;
	String testPATH;
	
	@Before
	public void setUp() {
		defaultConTest = new CLI();
		List<String> testListOfURLs = new ArrayList<String>();
		testPATH = "/home/uTest/cs350/Sum19/";
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/A/index.html");
		testListOfURLs.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/A/index.html");
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/D/testD.html");
		testListOfURLs.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		defaultConTest.setPATH(testPATH);
		defaultConTest.setListOfURLs(testListOfURLs);
	}
	
	@Test
	public void testCLI() throws IOException{
		CLI conTest = new CLI();
		List<String> actualListOfURLs = new ArrayList<String>();
		String actualPATH = defaultConTest.getPATH();
		boolean actualPathExists = defaultConTest.pathExists(actualPATH);
		boolean actualConfirmInput = defaultConTest.confirmInput();
		actualListOfURLs=defaultConTest.getListOfURLs();
		
		//assertThat(actualPATH, both(startsWith("/")).and((endsWith("/"))));
		assertThat(actualPATH, equalTo(testPATH));
		assertThat(actualListOfURLs.get(0), startsWith("https://"));
		assertThat(actualListOfURLs.get(0), either(endsWith(".html")).or((endsWith(".htm"))));
		assertThat(actualPathExists, either(is(true)).or(is(false)));
		assertThat(actualConfirmInput, either(is(true)).or(is(false)));
		assertThat(defaultConTest.toString(), containsString("https://"));
		assertThat(conTest.equals(defaultConTest), is(false));
		assertThat(conTest.hashCode(), not(equalTo(defaultConTest.hashCode())));
		
//		System.out.println(actualListOfURLs.get(0));
//		System.out.println(actualListOfURLs.get(1));
//		System.out.println(actualListOfURLs.get(2));
//		System.out.println(actualListOfURLs.get(3));
//		System.out.println(actualListOfURLs.get(4));
		
	}
	
	@Test
	public void testReadInput() {
		List<String> actualListOfURLs = new ArrayList<String>();
		String actualPATH = defaultConTest.getPATH();
		actualListOfURLs=defaultConTest.getListOfURLs();
		
		assertThat(actualPATH, both(startsWith("/")).and((endsWith("/"))));
		assertThat(actualListOfURLs.get(0), startsWith("https://"));
		assertThat(actualListOfURLs.get(0), either(endsWith(".html")).or((endsWith(".htm"))));
		assertThat(actualListOfURLs.get(3), either(endsWith(".html")).or((endsWith(".htm"))));
	}

	@Test
	public void testSetPATH() {
		CLI setPATHTest = new CLI();
		setPATHTest.setPATH("/home/root/uTest/cs350/Sum19/");
		String actualPATH = setPATHTest.getPATH();
		
		assertThat(actualPATH, both(startsWith("/")).and((endsWith("/"))));
	}
	
	@Test
	public void testSetListOfURLs() {
		CLI setListOfURLsTest = new CLI();
		List<String> testListOfURLs = new ArrayList<String>();
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/A/index.html");
		testListOfURLs.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/A/index.html");
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/D/testD.html");
		testListOfURLs.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		testListOfURLs.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		setListOfURLsTest.setListOfURLs(testListOfURLs);
		
		List<String> actualListOfURLs = new ArrayList<String>();
		actualListOfURLs=setListOfURLsTest.getListOfURLs();
		
		assertThat(actualListOfURLs.get(2), either(endsWith(".html")).or((endsWith(".htm"))));
		assertThat(actualListOfURLs.get(3), either(endsWith(".html")).or((endsWith(".htm"))));
		
	}
	
	@Test
	public void testSetOutputFiles() {
		CLI tester = new CLI();
        String testPath = "/home/uTest/cs350/Sum19/";
        List<String> fileNames = new ArrayList<String>();
        fileNames.add(testPath + "yyyymmdd-hhmmss-summary.txt");
        fileNames.add(testPath + "yyyymmdd-hhmmss-summary.xlsx");
        fileNames.add(testPath + "yyyymmdd-hhmmss-summary.json");
        
        assertEquals(fileNames.get(0), "/home/uTest/cs350/Sum19/yyyymmdd-hhmmss-summary.txt");
        assertEquals(fileNames.get(1), "/home/uTest/cs350/Sum19/yyyymmdd-hhmmss-summary.xlsx");
        assertEquals(fileNames.get(2), "/home/uTest/cs350/Sum19/yyyymmdd-hhmmss-summary.json");

	}
	
	@Test
	public void testPathExists() throws IOException{
		List<String> original = new ArrayList<String>();
		original.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/A/index.html");
		original.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/A/index.html");
		original.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/D/testD.html");
		original.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		original.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		
		String testPATH = "/home/uTest/cs350/Sum19/";
		
		CLI tester = new CLI ();
		tester.setListOfURLs(original);
		tester.setPATH(testPATH);
		tester.setListOfURLs(original);
		
		assertFalse(tester.pathExists(original.get(0)));
		assertTrue(tester.pathExists(original.get(1)));
		assertFalse(tester.pathExists(original.get(2)));
		assertFalse(tester.pathExists(original.get(3)));
		assertTrue(tester.pathExists(original.get(4)));
	}
	
	@Test
	public void testTranslate() throws IOException{
		List<String> original = new ArrayList<String>();
		original.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/A/index.html");
		original.add("https://secweb.cs.odu.edu/~uTest/cs350/Sum19/D/testD.html");
		original.add("https://www.cd.odu.edu/~uTest/cs350/Sum19/B/testB1.htm");
		
		List<String> translated = new ArrayList<String>();
		translated.add("A/index.html");
		translated.add("D/testD.html");
		translated.add("B/testB1.htm");
		
		String testPATH = "/home/uTest/cs350/Sum19/";
				
		File fileA = new File(original.get(0));
		File fileD = new File(original.get(1));
		File fileB = new File(original.get(2));
		
		CLI translateTester = new CLI ();
		translateTester.setListOfURLs(original);
		translateTester.setPATH(testPATH);
		
		assertEquals(translated.get(0),translateTester.translate(fileA));
		assertEquals(translated.get(1),translateTester.translate(fileD));
		assertEquals(translated.get(2),translateTester.translate(fileB));
		
	}
	
	@Test
	public void testSiteBoundaryCheck() throws IOException {
		SBC = new CLI();
		String path = "/src/test/resources/A";
		List<String> testListOfURLs = new ArrayList<String>();
		//This assumes URLs have been translated; down to just the local path to a file
		testListOfURLs.add("/HelloEarth.html");
		testListOfURLs.add("/HelloWorld.htm");
		testListOfURLs.add("/SubDirB/HiThere.php");
		testListOfURLs.add("/SubDirB/SubDirC/ContentHere.html");
		testListOfURLs.add("/SubDirB/NoContentHere.htm");
		Boolean actual = SBC.siteBoundaryCheck(path, testListOfURLs);
		assertThat(actual, is(true)); //All paths to files exists. Value should return as true 
		testListOfURLs.add("/B/WrongPath.html"); //Path does not exist as sub-directory in specified path
		actual = SBC.siteBoundaryCheck(path, testListOfURLs);
		assertThat(actual, is(false)); //The last added path is external. Value should return as false 
	}
	
//	@Test
//	public void testGetListOfURLs()
//	{
//		List<String> URLList;
//		File root = new File(testPATH);
//
//		for (File file : root.listFile())
//		{
//			if(file.isDirectory()) {}
//			else { URLList.add(file.getPath()); }
//		}
//		
//		for (String path : URLList)
//		{
//			assertThat(URLList.get(0), either(endsWith(".html")).or((endsWith(".htm"))));
//		}
//	}

}
