package edu.odu.cs.cs350;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.File;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TestFileGenerator {
	
	FileGenerator myFiles;
	List<String> somePages;

	@Before
	public void setUp() throws Exception {
		somePages = new ArrayList<String>();
		somePages.add("page1.html");
		somePages.add("page2.html");
		somePages.add("page3.html");
		myFiles = new FileGenerator(somePages);
		
		String path = System.getProperty("user.dir") + "/";
		myFiles.setRootPath(path);
	}

	@Test
	public void testFileGenerator() {
		assertThat(myFiles.pagesForTesting, is(somePages));
	}

	@Test
	public void testGenerateJSON() throws IOException {
		myFiles.generateJSON();
		assertTrue(myFiles.theJSON.exists());
		final Scanner scanner = new Scanner(myFiles.theJSON);
		String lineFromFile = scanner.nextLine();
		assertTrue(lineFromFile.contains("{"));
		
		//Test for an/Alternate/File.cpp
		Scanner myScan = new Scanner(myFiles.theJSON); // Open file into Scanner
		String jsonFile = myScan.useDelimiter("\\A").next(); // Convert Scanner into String
		
		assertTrue(jsonFile.contains("an/Alternate/File.cpp"));
	}

	/**
     * Tests basic functionality of generateExcel()
     * @throws IOException
	 * @throws Throwable
     */
    @Test
    public void testGenerateExcel() throws IOException, Throwable {
        myFiles.generateExcel();
        assertTrue(myFiles.theExcel.exists());
        XSSFWorkbook reader = XSSFWorkbookFactory.createWorkbook(myFiles.theExcel, true);
        assertThat(reader.getSheetAt(0).getRow(0).getCell(1).getStringCellValue(), is("# Images"));
        assertThat(reader.getSheetAt(0).getRow(0).getPhysicalNumberOfCells(), is(7));
        reader.close();
    }

	@Test
	public void testGenerateTxt() throws IOException {
		myFiles.generateTxt();
		assertThat(myFiles.theTXT.exists(), is(true));
		final Scanner scanner = new Scanner(myFiles.theTXT);
		int i = 0;
		while (scanner.hasNextLine()) {
		   final String lineFromFile = scanner.nextLine();
		   assertTrue(lineFromFile.contains(somePages.get(i)));
		   i++;
		}
	}
	
	@Test
	public void testFilePath() throws IOException{
		File img = new File("testImg.jpg");
		String expected = "testImg.jpg";
		assertEquals(expected,myFiles.filePath(img));
	}
	
	@Test
	public void testGenerateTimeStamp () {
		myFiles.generateTimeStamp();
		int year = Year.now().getValue();
		String actual = myFiles.getTimeStamp();
		assertThat(actual, containsString(Integer.toString(year)));
	}

	@Test
	public void testGetFiles () throws IOException {
		HTMLExtractor fileTest = new HTMLExtractor();
		fileTest.setFilePath("fileTesterHTML.html");
		fileTest.lookForHTML();
		
		HTMLExtractor fileTest2 = new HTMLExtractor();
		fileTest2.setFilePath("fileTesterHTML2.html");
		fileTest2.lookForHTML();
		
		List<HTMLExtractor> testPages = new ArrayList<HTMLExtractor>();
		testPages.add(fileTest);
		testPages.add(fileTest2);
		
		//being set to something random because getFiles() doesn't need this information
		String websitePath = "http://www.some_website_com";
		List<String> urlList = new ArrayList<String>();
		urlList.add("item1");
		urlList.add("item2");
		
		FileGenerator test = new FileGenerator(websitePath,testPages,urlList);
		test.getFiles();
		
		assertEquals(2,test.getFilesSize());
		assertEquals(0,test.getArchivesSize());
		assertEquals(3,test.getAudioSize());
		assertEquals(3,test.getVideoSize());
		assertEquals(0,test.getOtherSize());
	}
}
