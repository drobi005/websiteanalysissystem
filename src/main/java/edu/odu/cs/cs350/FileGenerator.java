package edu.odu.cs.cs350;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;

import org.apache.commons.math3.genetics.GeneticAlgorithm;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.cedarsoftware.util.io.JsonObject;
import com.cedarsoftware.util.io.JsonWriter;

/**
 * This class takes the data obtained from HTMLExtractor and prints it
 * into the appropriate files (JSON, Excel, text).
 * 
 * JSON file
 * 	One entry per page detailing:
 *		Number of local images
 *		Number of external images
 *		Number of scripts referenced
 *  	Number of stylesheets utilized
 *		Listing of images
 *  	Listing of scripts
 *  	Listing of stylesheets
 *  	Number of intra-page links
 *  	Number of intra-site links
 *  	Number of external links
 *  	One entry per image detailing:
 *  	Number of pages on which it is displayed
 *  	Listing of pages on which it is displayed
 *  One entry per archive file detailing
 *  	File size
 *  	Path to resource (relative to local site root)
 *  One entry per video file detailing
 *  	File size
 *  	Path to resource (relative to local site root)
 *  One entry per audio file detailing
 *  	File size
 *  	Path to resource (relative to local site root)
 *  One entry per non-categorized file detailing
 *  	File size
 *  	Path to resource (relative to local site root)
 *  
 * Text file
 * 	size	page
 * 
 * Excel file
 * 	Page	#images	#CSS	Scripts	#Links(intra) #Links(internal)	#Links(external)
 * 	One row per internal page.
 */

public class FileGenerator{
	public File theJSON;
	public File theExcel;
	public File theTXT;
	private List<HTMLExtractor> pagesToPrint = new ArrayList<HTMLExtractor>();
	private String timestamp;
	private String rootPath;
	private List<String> listURLs = new ArrayList<String>();
	
	//For testing purposes, until HTMLExtractor is further implemented
	public List<String> pagesForTesting;
	
	private Map<String, List<HTMLExtractor.Media>> files = new HashMap<String, List<HTMLExtractor.Media>>();
	private List<HTMLExtractor.Media> archives;
	private List<HTMLExtractor.Media> audio;
	private List<HTMLExtractor.Media> video;
	private List<HTMLExtractor.Media> other;
	private List<HTMLExtractor.Image> images;
	
	
	//size getters for the lists above
	public int getFilesSize() {return files.size();}
	public int getArchivesSize() {return archives.size();}
	public int getAudioSize() {return audio.size();}
	public int getVideoSize() {return video.size();}
	public int getOtherSize() {return other.size();}
	
	/**
	 * Default constructor
	 * Initialize class members
	 */
	public FileGenerator()
	{
		archives  = new ArrayList<HTMLExtractor.Media>();
		audio = new ArrayList<HTMLExtractor.Media>();
		video = new ArrayList<HTMLExtractor.Media>();
		other = new ArrayList<HTMLExtractor.Media>();
		images = new ArrayList<HTMLExtractor.Image>();
	}
	
	/**
	 * Constructor to be used with WAS class
	 * 
	 * @param websitePath root path of the analyzed website
	 * @param pages the list of pages that were analyzed by the HTMLExtractor class
	 * @param url the user input provided URLs
	 */
	public FileGenerator(String websitePath, List<HTMLExtractor> pages, List<String> url)
	{
		rootPath = websitePath;
		pagesToPrint = pages;
		listURLs = url;
		archives  = new ArrayList<HTMLExtractor.Media>();
		audio = new ArrayList<HTMLExtractor.Media>();
		video = new ArrayList<HTMLExtractor.Media>();
		other = new ArrayList<HTMLExtractor.Media>();
		images = new ArrayList<HTMLExtractor.Image>();
	}
	
	/**
	 * Testing constructor
	 * 
	 * Test a list of provided page names before full integration
	 * of the class into the WebsiteAnalysis system.
	 * @param pages
	 */
	public FileGenerator(List<String> pages)
	{
		pagesForTesting = pages;
		archives  = new ArrayList<HTMLExtractor.Media>();
		audio = new ArrayList<HTMLExtractor.Media>();
		video = new ArrayList<HTMLExtractor.Media>();
		other = new ArrayList<HTMLExtractor.Media>();
		images = new ArrayList<HTMLExtractor.Image>();
		generateTimeStamp();
	}
	
	/**
	 * Sets list of page information to be printed
	 * @param analyzedPages
	 */
	public void setPagesToPrint(List<HTMLExtractor> analyzedPages)
	{
		pagesToPrint = analyzedPages;
	}
	
	/**
	 * sets root path of the directory
	 * @param path
	 */
	public void setRootPath(String path) {
		rootPath = path;
	}
	
	/**
	 *
	 * @return the root path of the directory
	 */
	public String getRootPath() {
		return rootPath;
	}
	
	/**
	 * Generate a timestamp, in military time.
	 * Format: YYYYMMDD-HHssmm-summary
	 */
	public void generateTimeStamp() {
		SimpleDateFormat dateTimeInGMT = new SimpleDateFormat("yyyyMMdd-HHmmss");
		String ts = dateTimeInGMT.format(new Date());
		ts = ts + "-summary";
		this.timestamp = ts;
		//System.out.println("Timestamp: "+ts); //For testing purposes
	}
	
	/**
	 * Retrieve timestamp
	 * @return String timestamp
	 */
	public String getTimeStamp() {
		return timestamp;
	}
	
	/**
	 * Generates the JSON file and fills with data from the list of analyzed pages.
	 * 
	 * @throws IOException
	 */
	public void generateJSON() throws IOException
	{	
		// Function will iterate through List<HTMLExtractor> to print page by page info
		theJSON = new File(getTimeStamp()+".json");
		if (theJSON.exists()) 
		{
					theJSON.delete();
		}
				
		theJSON.createNewFile();
		FileWriter jsonWriter = new FileWriter(theJSON, true);
		PrintWriter jsonWriting = new PrintWriter(jsonWriter);

		// For testing purposes
		HTMLExtractor myExtractorTags = new HTMLExtractor();
		myExtractorTags.setFilePath("testTagsHTML.html");
		myExtractorTags.lookForHTML();
		// End for testing purposes
				
		Map map = new HashMap(); // Map for printing options
		map.put((JsonWriter.TYPE), false); // Omit "@type"
		map.put(JsonWriter.PRETTY_PRINT, true); // Format for easy testing
		
	    Map nameBlackList = new HashMap(); // Map for black listed variables
	    List<String> blackList = new ArrayList(); // List of field names to black list
	    
	    // Fields that will not be printed
	    blackList.add("images"); // Image printing must be site-wide, not per page. To be implemented later.
	    blackList.add("scripts");
	    blackList.add("sheets");
	    blackList.add("links");
	    blackList.add("archive");
	    blackList.add("videos");
	    blackList.add("audio");
	    blackList.add("other");
	    blackList.add("willBeAnalyzed");
	    blackList.add("rootPath");
	    // End to not be printed
	    
	    nameBlackList.put(HTMLExtractor.class, blackList); // Attach list to class
	    map.put(JsonWriter.FIELD_NAME_BLACK_LIST, nameBlackList);
		
	    
		// WIP	
		/*String thePath = JsonWriter.objectToJson(rootPath);
		jsonWriting.println(thePath);
		
		String theURLs = JsonWriter.objectToJson(listURLs);
		jsonWriting.println(theURLs);*/
	    // WIP
		
	    
		//  For testing purposes
		pagesToPrint.add(myExtractorTags);
		// When integrated, pagesToPrint will be filled from the constructor
		
		// Iterate through all pages
		for(HTMLExtractor onePage : pagesToPrint)
		{
			String json2 = JsonWriter.objectToJson(onePage, map); // Test HTMLExtractor object	
			jsonWriting.println(json2);
		}
		// Finished printing per page data
		
		// Begin printing site-wide data
		
		// New map necessary for List<Object>, cannot reuse HTMLExtractor map
		Map objMap = new HashMap();
		objMap.put(JsonWriter.TYPE, false);
		objMap.put(JsonWriter.PRETTY_PRINT, true);
		
		getImages(); // Get site-wide image list
		
		String allImages = JsonWriter.objectToJson(images, objMap);
		jsonWriting.println(allImages);
        
        getFiles(); // Get site-wide file totals
		
		String allFiles = JsonWriter.objectToJson(files, objMap); // Add to bottom of json
		jsonWriting.println(allFiles);
		
		jsonWriting.close();
	}
	
	/**
	 * Generates an .xlsx Excel workbook with data from analysis.
	 * @throws IOException
	 */
	public void generateExcel() throws IOException
	{
	    theExcel = new File(getTimeStamp()+".xlsx");
        //theExcel = new File("theExcel.xlsx");
	    
	    HTMLExtractor src = new HTMLExtractor();
        src.setFilePath("testTagsHTML.html");
        src.lookForHTML();
        pagesToPrint.add(src);
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("summary");
        
        XSSFFont headerFont = workbook.createFont();
        headerFont.setBold(true);
        
        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setFont(headerFont);
        
        XSSFRow headerRow = sheet.createRow(0);
        headerRow.setRowStyle(headerStyle);
        
        String headers[] = {"Page", "# Images", "# CSS", "Scripts", "#Links (Intra-Page)",
                "# Links (Internal)", "# Links (External)"};
        
        // output headers
        for (int i = 0; i < headers.length; i++) {
            headerRow.createCell(i).setCellValue(headers[i]);
            headerRow.getCell(i).setCellStyle(headerStyle);
            sheet.autoSizeColumn(i);
        }
        
        // fill in analysis data
        XSSFRow dataRow;
        int nextRow = 1;
        
        /*
        // for testing
        for (String pg : pagesForTesting) {
            dataRow = sheet.createRow(nextRow);
            dataRow.createCell(0).setCellValue("One");
            dataRow.createCell(1).setCellValue(2);
            dataRow.createCell(2).setCellValue(3);
            dataRow.createCell(3).setCellValue(4);
            dataRow.createCell(4).setCellValue(5);
            dataRow.createCell(5).setCellValue(6);
            dataRow.createCell(6).setCellValue(7);
            nextRow++;
        }
        */
        
        for (HTMLExtractor pg : pagesToPrint) {
            dataRow = sheet.createRow(nextRow);
            dataRow.createCell(0).setCellValue(pg.getFilePath());
            dataRow.createCell(1).setCellValue(pg.getNumImages());
            dataRow.createCell(2).setCellValue(pg.getNumStylesheets());
            dataRow.createCell(3).setCellValue(pg.getNumScripts());
            dataRow.createCell(4).setCellValue(pg.getIntraPageLinks());
            dataRow.createCell(5).setCellValue(pg.getInternalImages());
            dataRow.createCell(6).setCellValue(pg.getExternalLinks());
            nextRow++;
        }
        
        for (int i=0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }
        
        FileOutputStream out = new FileOutputStream(theExcel);
        workbook.write(out);
        workbook.close();
	}
	
	/**
	 * Generates the txt file with a list of analyzed pages.
	 * @throws IOException
	 */
	public void generateTxt() throws IOException
	{
		theTXT = new File(getTimeStamp()+".txt");
		if (theTXT.exists()) 
		{
		     theTXT.delete();
		}
		
		theTXT.createNewFile();
		FileWriter txtWriter = new FileWriter(theTXT, true);
		PrintWriter txtWriting = new PrintWriter(txtWriter);
		for(int i = 0; i < pagesForTesting.size(); i++)
		{
			txtWriting.println(pagesForTesting.get(i));
		}
		txtWriting.close();
	}
	
	/**
	 * This function returns file path of any type of file
	 * that is to be used in the output files
	 * @param file
	 */
	public String filePath(File file) throws IOException
	{
		String path = file.getCanonicalPath();
    	return path.substring(rootPath.length());
	}
	
	/**
	 * This function assigns archive,audio, video, and other arrayLists
	 * The values for these arrayLists are got from the different pages of the website
	 * (pagesToPrint) arrayList
	 */
	public void getFiles()
	{
		for(int i = 0; i< pagesToPrint.size(); i++)
		{
			//getting archives
			for(int j = 0; j< pagesToPrint.get(i).getNumArchive(); j++)
			{
				if(!(archives.contains(pagesToPrint.get(i).getArchiveContent(j))))
				{
					archives.add(pagesToPrint.get(i).getArchiveContent(j));
				}
			}
			
			//getting audio
			for(int k = 0; k< pagesToPrint.get(i).getNumAudio(); k++)
			{
				if(!(audio.contains(pagesToPrint.get(i).getAudioContent(k))))
				{
					audio.add(pagesToPrint.get(i).getAudioContent(k));
				}
			}
			
			//getting video
			for(int l = 0; l< pagesToPrint.get(i).getNumVideo(); l++)
			{
				if(!(video.contains(pagesToPrint.get(i).getVideoContent(l))))
				{
					video.add(pagesToPrint.get(i).getVideoContent(l));
				}
			}
			
			//getting other
			for(int m = 0; m< pagesToPrint.get(i).getNumOther(); m++)
			{
				if(!(other.contains(pagesToPrint.get(i).getOtherContent(m))))
				{
					other.add(pagesToPrint.get(i).getOtherContent(m));
				}
			}
		}
		
		//add all the different types of files to files list
		if(archives.size() > 0) 
		{
			files.put("archive", archives);
		}
		if(audio.size() > 0) 
		{
			files.put("audio", audio);
		}
		if(video.size() > 0) 
		{
			files.put("video", video);
		}
		if(other.size() > 0) 
		{
			files.put("other", other);
		}
	}
	
	/**
	 * Images from the different pages in the website are taken and placed into a single list,
	 * so that all images in the website can be listed together.
	 */
	public void getImages()
	{
		for(int i = 0; i < pagesToPrint.size(); ++i)
		{
			for(int j = 0; j < pagesToPrint.get(i).getNumImages(); ++j)
			{
				if(!(images.contains(pagesToPrint.get(i).getImageContent(j))))
				{
					images.add(pagesToPrint.get(i).getImageContent(j));
				}
			}
		}
	}
}

