package edu.odu.cs.cs350;

import java.util.List;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.event.ListSelectionEvent;

/**
 * This class is intended to take in a path leading to the directory a
 * local copy of a website is located.
 * Followed by 1 or more URLs
 * The syntax: PATH URL1 URL2 ... URL(N)
 * 
 * Error checks pertaining to invalid input in the command line will occur
 * in this class.
 * 
 * @author Avocado3
 */

public class CLI {
	private String path;
	private List<String> listOfURLs = new ArrayList<String>();
    private List<String> outputFiles = new ArrayList<String>();
	
	/**
	 * Initialize the command line interface fields
	 * <ul>
	 * <li> Set the PATH to proper directory path.</li>
	 * <li> Set the listOfURLs with N amount of URL entries. </li>
	 * </ul>
	 */
	public CLI() {
		//Default constructor
	}
	
	/**
	 * Initialize the command line interface fields from 
	 * provided arguments.
	 * 
	 * @param aPATH the path to the local copy of a site
	 * @param alistOfURLs a container of URLs
	 */
//	public CLI(String aPATH, List<String> alistOfURLs) { 
//		this.PATH = aPATH;
//		//this.listOfURLs = alistOfURLs;
//		for (int i=0; i<alistOfURLs.size(); i++) {
//			this.listOfURLs.add(i, alistOfURLs.get(i));
//		}
//	} // **May not need this non-default constructor**
	
	/**
	 * Set the PATH field
	 */
	public void setPATH(String aPATH) {
		this.path = aPATH;
	}
	
	/**
	 * Retrieve the PATH contents
	 * 
	 * @return PATH as a `String`
	 */
	public String getPATH() {
		return this.path;
	}
	
	
	/**
	 * Set the list of URLs
	 */
	public void setListOfURLs(List<String> alistOfURLs){
		this.listOfURLs.clear(); //empty the list
		for (int i=0; i<alistOfURLs.size(); i++) {
			this.listOfURLs.add(i, alistOfURLs.get(i));
		}
	}
	
	/**
	 * Retrieve the list of URLs
	 * 
	 * @return listOfURLs as a `List<String>`
	 */
	String subPATH; //subdirectories being browsed
	public List<String> getListOfURLs()
	{
		//Note: This function should ONLY return the list of URLs.
		//      It should NOT check if the PATH or listOfURL contents are valid.
		//      Everything commented should be moved to into a path/URL checker
		//      method.
		
//		File root = new File(PATH);
//		
//		for (File file : root.listFiles())
//		{
//			if (file.isDirectory())
//			{
//				subPATH = file.getName();
//				getListOfURLs();
//			}
//			else
//			{
//				listOfURLs.add(file.getPath());
//			}
//		}
		
		return listOfURLs;
	}
	
	/**
	 * This method is used to store the entered path and URL(s) in one string.
	 * This method will initially store the first substring in the PATH field.
	 * Then add URLs into the listOfURLs container until the end of the string
	 * has been reached.
	 */
	public void readInput(String[] args) {
		if (args.length==0) {errorSyntax(0);}
		else if (args.length==1) {errorSyntax(1);}
		else {
			path = args[0];
			for(int i=1; i<args.length;i++) {listOfURLs.add(args[i]);}
			
			//For Testing Purposes
			//System.out.println(PATH);
			//for(String input: listOfURLs) {System.out.println(input);}
		}
	}
	
	/**
	 * Check if the input conforms to proper syntax and is valid.
	 * If the input in invalid, call the appropriate error checking method from
	 * the ErrorChecker class to alert the user of proper syntax for this software. 
	 * 
	 * Check if the PATH has been correctly entered.
	 *      If no input has been provide after pressing enter, call the 
	 *      errorSyntax method from the ErrorChecker class to inform user
	 *      to follow proper syntax.
	 * Check if 1 or more URLs have been provided
	 *      If no URLs have been entered, call the errorURLsSize method from the 
	 *      ErrorChecker class to inform user to provide at least 1 URL
	 *If there are no errors, return true and then proceed to the Website Analysis 
	 *section of the software.
	 * @return True or False as a `boolean`
	 */
	public boolean confirmInput() {
	    File pathCheck = new File(path);
	    if (!pathCheck.exists()) {this.errorMissingDirectory(pathCheck); return false;}
	    
	    else if (listOfURLs.isEmpty()) {this.errorMissingURLs(); return false;}
	    
	    for (String url : listOfURLs) {
	        File urlCheck = new File(url);
	        if (!urlCheck.exists()) {this.errorInvalidURL(url);}
	        return false;
	    }
	    
	    // if function got this far, input should be good
		return true;
	}
	
	/**
	 * After creation of output files, record their file path locations
	 */
	public void setOutputFilePaths(String txt, String xlsx, String json) {
	    outputFiles.add(txt);
	    outputFiles.add(xlsx);
        outputFiles.add(json);
	}
	
	/**
	 * listing of output files from analysis, one per line
	 */
	public void getOutputFilePaths() {
	    for (String i : outputFiles) {System.out.println(i);}
	}
	
	/** 
	 * Only messages on CLI window will be usage, error, and output messages.
	 */
	public void printUsageMessages() {
	    System.out.println("System Usage Syntax: PATH URL1 URL2...URL(N)");
	    System.out.println("PATH: path to local copy of webpage");
	    System.out.println("URL:  one or more files for analysis");
	}
	
	/**
	 * Output an error message telling the user that the input does not follow the syntax
	 * formatting of the system.
	 * Output to terminal the proper syntax and inform of a --help command
	 */
	public void errorSyntax(int n) {
		System.out.println("Error: improper syntax");
		if (n==0) {System.out.println("No path to local directory has been entered\n");}
		if (n==1) {System.out.println("No URL(s) have been entered\n");}
		this.printUsageMessages();
	}	
	
	/**
	 * Output an error message telling the user that no URLs have been provided
	 */
	public void errorMissingURLs() {
		System.out.println("Error: no URLs provided");
		this.printUsageMessages();
	}
	
	/**
	 * The system must output an explanatory error message when a path to a non-existent
	 * directory is input
	 */
	public void errorMissingDirectory(File filePath) {
	    System.out.println("Error: " + path + " is not a valid directory.");
	}
	
	/**
	 * The system must output and explanatory error message when a path to a non-existent
	 * URL is input
	 */
	public void errorInvalidURL(String URL) {
	    System.out.println("Error: " + URL + " is not a valid URL.");
	}
	
	/**
	 * List all URLs provided in the listOfURLs container
	 * 
	 */
	@Override
	public String toString() {
		return "CLI [listOfURLs=" + listOfURLs + "]";
	}
	
	/**
	 * Generate Hashcode for CLI class 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((listOfURLs == null) ? 0 : listOfURLs.hashCode());
		return result;
	}
	
	
	/**
	 * Generate clone of CLI class
	 */
	@Override
	public Object clone() {
		CLI copyCLI = new CLI();
		copyCLI.setPATH(this.path);
		copyCLI.setListOfURLs(this.listOfURLs);
		return copyCLI;
	}

	/**
	 * Generate equals method
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CLI other = (CLI) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (listOfURLs == null) {
			if (other.listOfURLs != null)
				return false;
		} else if (!listOfURLs.equals(other.listOfURLs))
			return false;
		return true;
	}

	/*
	 * This function checks for if the path exists in the listOfURLS.
	 * True is returned if the path exists in the list, false otherwise.
	 */
	public boolean pathExists(String check) throws IOException{
		File file = new File(check);
		boolean value = false;
		for(int i = 0; i<listOfURLs.size(); i++)
		{
			if(translate(file) == listOfURLs.get(i))
			{
				value = true;
			}
		}
		return value;
	}
	
	
	/*
	 * This function translates each URL to a local file structure and
	 * returns a path with the root stripped off of it.
	 */
	public String translate(File file) throws IOException{
		String filePath = file.getCanonicalPath();
		String value = filePath;
		if(filePath.contains(path))
		{
			value = filePath.substring(path.length());
		}else {
			for(int i = 0; i<listOfURLs.size(); i++)
			{
				if(filePath.contains(listOfURLs.get(i))) {
					value = filePath.substring(listOfURLs.get(i).length());
				}
			}
		}
		return value;
	}
	
	/*
	 * This method is used to check if all provided URLs reference resources
	 * within the provided main directory.
	 * If ALL URL paths lead to files within the main directory, sub-directories of 
	 * the main included, this method will return as true.
	 * If the URL paths do not exist in the main directory, the site boundary check 
	 * will stop and return as false. 
	 */
	public boolean siteBoundaryCheck(String aPath, List<String> aListOfURLs) throws IOException {
		File absoluteDirectory = new File(""); //generate absolute path to current working directory
		File mainDirectory = new File(absoluteDirectory.getAbsolutePath()+aPath); 
		//System.out.println(mainDirectory.getAbsolutePath()); //For testing purposes
		String relPath;
		for(String URL : aListOfURLs) {
			if (!(mainDirectory = new File(absoluteDirectory.getAbsolutePath()+aPath+URL)).isFile()) {
				//relPath = mainDirectory.getCanonicalPath(); //Results in full path display
				relPath = new File(aPath+URL).getCanonicalPath();
				System.out.println(relPath+ ", does not exit in main directory");
				return false;
			}
			else {
				//For testing purposes
				//System.out.println("Main Directory = "+mainDirectory.getAbsolutePath()+aPath);
				//System.out.println(mainDirectory.getCanonicalPath()+aPath+URL);
			}
		}
		return true;
	}
}
	
