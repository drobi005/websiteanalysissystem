package edu.odu.cs.cs350;

import java.io.IOException;

/**
 * This class serves as the main class of the Website Analysis system
 * This class is intended to call the runWebsiteAnalysisSystem method,
 * via a WebsiteAnalysisSystem object.
 * 
 * @author Avocado3
 */
public class Main {

	public static void main(String[] args) throws IOException {
		WebsiteAnalysisSystem webSystem = new WebsiteAnalysisSystem();
		webSystem.runWebsiteAnalysisSystem(args);
	}

}
