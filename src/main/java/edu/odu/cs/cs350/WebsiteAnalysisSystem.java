package edu.odu.cs.cs350;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is intended to instantiate objects of the Website class,
 * the HTMLExtractor class, and the CLI class.
 * 
 * Each object will run the appropriate functions to generate data.
 * 
 * Data generated from each object's attributes will be store in 
 * the appropriate fields of this class.
 * @author Avocado3
 *
 */
public class WebsiteAnalysisSystem {
	private String localPath;
	private String timestamp;
	private List<String> webURLs = new ArrayList<String>();
	private List<HTMLExtractor> allPages;
	private List<List<String>> imagesFromPages = new ArrayList<List<String>>();
	
	//private String[] args;
	/**
	 * Default constructor.
	 * No fields need to be initialized here.
	 */
	public WebsiteAnalysisSystem() {}
	
	/**
	 * This method will be used to instantiate objects of Website,
	 * HTMExtractor, and CLI classes.
	 * 
	 * This method utilizes those objects to generate and store data
	 * in the appropriate fields in this class
	 * @throws IOException 
	 */
	public void runWebsiteAnalysisSystem(String[] args) throws IOException {
		// WIP
		//Appropriate variables and objects to be added here
		CLI cli = new CLI();
		File file;
		String URL;
		StringBuilder sb;
		HTMLExtractor html = new HTMLExtractor();
		int pageImageTally;
		int pageCSSTally;
		int pageScriptTally;
		
		cli.readInput(args); //Take in Path input and perform Error Check
		this.localPath=cli.getPATH(); 
		this.webURLs=cli.getListOfURLs();
		//cli.setListOfURLs(null);
		
		System.out.println(cli.getListOfURLs());
		for(int i=0; i < webURLs.size(); i++) {
			file = new File(webURLs.get(i));
			webURLs.set(i, URL=cli.translate(file)); //Translate URL Path and update current element
			sb = new StringBuilder(URL);
			sb.delete(0, 3); //Remove the Drive letter, Colon, and back slash
			sb.insert(0, '/'); //change initial character to forward slash
			webURLs.set(i, sb.toString()); //Re-update current element
			//System.out.println(webURLs.get(i));
		}
		cli.setListOfURLs(webURLs); //set the URLs list
		if (cli.siteBoundaryCheck(this.localPath, this.webURLs) == false) {
			System.exit(0); //Terminate program
		}
		else {
			//For testing purposes
//			System.out.println(this.localPath);
//			System.out.println(this.webURLs.get(0));
//			System.out.println(this.webURLs.get(1));
//			System.out.println(this.webURLs.get(2));
			
			
			//For testing purposes
	//		List<String> a = new ArrayList<String>();
	//		a = cli.getListOfURLs();
	//		for(String input: a) {System.out.println(input);}
			

			//For each page to be analyzed, fill the List<HTMLExtractor> allPages
			//while (pages to be analyzed)
			//	HTMLExtractor current = new HTMLExtractor();
			//	current.setFilePath(aPath)
			//	current.analyzeTheHTML();
			//	allPages.put(current);
			
			// Pass the website path, analyzed pages, and list of URLs to FileGenerator
			// FileGenerator generate = new FileGenerator(localPath, allPages, webURLs);
			FileGenerator generate = new FileGenerator(); //placeholder
			generate.generateTimeStamp();
			this.timestamp = generate.getTimeStamp();
			
			// Generate the files
			// generate.generateTXT();
			// generate.generateJSON();
			// generate.generateExcel();
			
			
			
			
			
			pageImageTally = html.getNumImages();
			pageCSSTally = html.getNumStylesheets();
			pageScriptTally = html.getNumScripts();
			
			for (String path : html.imageAbsPaths) //for each image in the file
			{
				//we assume here that the current image
				//doesn't exist in the master list.
				boolean exist = false;
				for (List<String> page : imagesFromPages) //for each entry in the imagefile master list
				{
					if (page.get(0) == path) //if we get a match
					{
						//we have a match, so we don't need this anymore
						exist = true;
						boolean check = true;
						//check to make sure the filepath doesn't already exist in the list
						for (String file_ : page) 
						{
							if (path == file_)
							{
								//if this is ever set, we'll know that
								//the filepath already exists there
								check = false; 
							}
						}
						if (check)
						{
							page.add(html.getFilePath()); //add filepath to list, for that image
						}
					}
				}
				
				if (!exist)
				{
					//add a new list, with imagepath at the front
					List<String> newEntry = new ArrayList<String>();
					newEntry.add(path);
					imagesFromPages.add(newEntry);
				}
			}
		}
	}
}
