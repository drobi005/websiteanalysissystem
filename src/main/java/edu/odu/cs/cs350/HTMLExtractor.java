package edu.odu.cs.cs350;

import java.io.File; 







import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Attribute;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.*;

import org.jsoup.nodes.Element;

import java.util.regex.Pattern;
import java.io.InputStream;
import java.lang.ClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * This class is intended to take any file as input and check if there is HTML content.
 * 
 * The HTML content is then extracted for analysis and is sorted into the appropriate collection.
 * 
 *  * @author Avocado3
 */
public class HTMLExtractor
{
	private String path;
	
	// The following is for easy JSON formatting
	Map<String, Integer> imageCount  = new HashMap<String, Integer>();
	Map<String, Integer> jsCount  = new HashMap<String, Integer>();
	Map<String, Integer> cssCount  = new HashMap<String, Integer>();
	List<String> imageAbsPaths;
	List<String> scriptPaths;
	List<String> cssPaths;
	Map<String, Integer> linkCount  = new HashMap<String, Integer>();
	// End for formatting
	
	private List<Image> images;
	//private int numInternalImages;
	private List<ScriptOrSheet> scripts;
	private List<ScriptOrSheet> sheets;
	private List<Link> links;
	private List<Media> archive;
	private List<Media> videos;
	private List<Media> audio;
	private List<Media> other;
	
	private boolean willBeAnalyzed;
	private String rootPath;
	
	static class Image{
		public Image(double newFileSize, String newURI) {
			fileSize = newFileSize;
			URI = newURI;
			
			// pagesUtilizedOn to be implemented later
		}
	
		private double fileSize;
		private String URI;
		private List<String> pagesUtilizedOn;
		
		public double getFileSize() { return fileSize;}
		public String getURI() { return URI;}
	}
	
	static class ScriptOrSheet{
		public ScriptOrSheet(String newURI) {
			URI = newURI;
			
			// pagesUtilizedOn to be implemented later
		}
		
		private String URI;
		private List<String> pagesUtilizedOn;
		public String getURI() { return URI;}
	}
	
	static class Link{
		public Link(String newURL) {
			URL = newURL;
		}
		
		private String URL;  
		
		public String getURL() { return URL;}
	}
	
	static class Media{
		public Media(double newFileSize, String newPathToResource) {
			fileSize = newFileSize;
			pathToResource = newPathToResource;
		}
		
		private double fileSize;  
		private String pathToResource;
		
		public double getFileSize() { return fileSize;}
		public String getPathToResource() { return pathToResource;}
	}
	
    public int getNumImages() { return images.size();}
    
    public int getInternalImages() { return imageCount.get("local");}
    
    public int getExternalImages() { return imageCount.get("external");}
    
    public int getNumStylesheets() { return sheets.size();}
    
    public int getNumScripts() { return scripts.size();}
    
    public int getNumLinks() { return links.size();}
    
    public int getIntraPageLinks() { return linkCount.get("intra-page");}
    
    public int getIntraSiteLinks() { return linkCount.get("intra-site");}
    
    public int getExternalLinks() { return linkCount.get("external");}
    
    public int getNumArchive() {return archive.size();}
    
    public int getNumVideo() { return videos.size();}
    
    public int getNumAudio() { return audio.size();}
    
    public int getNumOther() { return other.size();}
    
    public String getFilePath() { return path;}
    
    public void setFilePath(String thePath) { path = thePath;}
    
    public boolean getWillBeAnalyzed() { return willBeAnalyzed;}
    
    public Image getImageContent(int i) { return images.get(i);}
    
    //Getters for the different file lists
    public Media getArchiveContent(int i) {return archive.get(i);}
    public Media getAudioContent(int i) {return audio.get(i);}
    public Media getVideoContent(int i) {return videos.get(i);}
    public Media getOtherContent(int i) {return other.get(i);}
    
    /**
     * Returns root path of the local file directory
     * 
     * @return String
     */
    public String getRootPath() { return rootPath;}
    
    /**
     * Sets the root path of the directory
     * 
     * @param aPath of the local file directory
     */
    public void setRootPath(String aPath) { rootPath = aPath;}
	
	/**
	 * Begin new HTML extraction for a file
	 */
	public HTMLExtractor()
	{
		path = "";
		images = new ArrayList();
		scripts = new ArrayList();
		sheets = new ArrayList();
		links = new ArrayList();
		archive = new ArrayList();
		videos = new ArrayList();
		audio = new ArrayList();
		other = new ArrayList();
	}
    
    /**
     * Open file then search for HTML content.
	 * 
	 * HTML tags will be extracted for analysis if file exists and does not give a 404 error.
	 */
    public void lookForHTML() throws IOException
    {
    	// Create file from given path
		File myFile = new File(getClass().getClassLoader().getResource(path).getFile());
		
    	if(notError404Checker(myFile))
    	{
    		Scanner myScan = new Scanner(myFile); // Open file into Scanner
    		String myHTML = myScan.useDelimiter("\\A").next(); // Convert Scanner into String
    		boolean isHTML = myHTML.contains("<!DOCTYPE html>"); // Check if proper HTML
    		
    		if(isHTML) // If <!DOCTYPE html> was found, then proper HTML
    		{	
    			Document currentPage = Jsoup.parse(myHTML);
            	willBeAnalyzed = true; // It was determined that HTML was found
        		// Begin analysis here
        		analyzeTheHTML(currentPage);
    		}
    		else
    		{
    			willBeAnalyzed = false; // HTML was not found
    		}
    	}
    	else
    	{
    		willBeAnalyzed = false; // Page was a 404 error
    	}
    }
    
    /**
     * Look through extracted tags and sort accordingly into the lists: images, stylesheets, scripts, etc.
     */
    public void analyzeTheHTML(Document currentPage)
    {
    	int numInternal = 0;
    	int numExternal = 0;
    	
        Elements extractedImages = currentPage.getElementsByTag("img");
        for(Element aImage : extractedImages)
        {
        	String abspath = getImageAbsPath(aImage.attr("src"));
        	imageAbsPaths.add(abspath);
        	int index1 = path.indexOf('/') + 1; //finds second slash
        	int index2 = path.indexOf('/', index1); //finds third slash.
        	String domainName = path.substring(index1 + 1, index2);
        	
        	if (classifyImage(aImage.attr("src")) && !abspath.contains(domainName))
        		numInternal++;
        	else
        		numExternal++;
        	
        	// Attempt to fix getting image path
        	/*URL url = null;
        	try {
        		url = new URL(aImage.attr("src"));
        		String path = url.getPath();
        		imageAbsPaths.add(path);
        	}
            catch (Exception e) { 

            }*/
        	
        	
        	// File size detection not yet implemented
        	// Set to 5 as default
        	double newFileSize = 5;
        	
        	Image newImage = new Image(newFileSize, aImage.attr("src"));
        	images.add(newImage);
        }
        // Set data for JSON printing
        imageCount.put("local", numInternal);
        imageCount.put("external", numExternal);
        
        numInternal = 0; // Reset for next HTML tag
        numExternal = 0;
        
        Elements extractedStyleSheets = currentPage.select("link[rel='stylesheet']");
        for(Element aStyleSheet : extractedStyleSheets)
        {
        	boolean isInternal;
        	int index1 = path.indexOf('/') + 1; //finds second slash
        	int index2 = path.indexOf('/', index1); //finds third slash.
        	String domainName = path.substring(index1 + 1, index2);
        	
        	String sheetURL = aStyleSheet.attr("href");
        	if (sheetURL.contains("http") && !sheetURL.contains(domainName)) //&& !sheetURL.contains("native domain name")) // Commented until further implemented
        		numExternal++;
        	else
        		numInternal++;
        	
        	ScriptOrSheet newStylesheet = new ScriptOrSheet(aStyleSheet.attr("href"));
        	sheets.add(newStylesheet);
        }
        jsCount.put("local", numInternal);
        jsCount.put("external", numExternal);
        
        numInternal = 0; // Reset for next HTML tag
        numExternal = 0;
        
        Elements extractedScripts = currentPage.select("script[type='text/javascript']");
        for(Element aScript : extractedScripts)
        {
        	boolean isInternal;
        	
        	String scriptURL = aScript.attr("src");
        	if (scriptURL.contains("http")) //&& !scriptURL.contains("native domain name")) // Commented until further implemented
        		numExternal++;
        	else
        		numInternal++;
        	
        	ScriptOrSheet newScript = new ScriptOrSheet(aScript.attr("href"));
        	scripts.add(newScript);
        }
        cssCount.put("local", numInternal);
        cssCount.put("external", numExternal);
        
        numInternal = 0; // Reset for next HTML tag
        numExternal = 0;
    	int numInternalPage = 0; // Third option for link classification
    	
        Elements extractedLinks = currentPage.select("a[href]");  
        for (Element aLink : extractedLinks)
        {  
        	
            String theClassification = classifyLink(aLink.attr("href"));
            if(theClassification == "external")
            	numExternal++;
            else if(theClassification == "intra-page")
            	numInternalPage++;
            else
            	numInternal++;
            
            Link newLink = new Link(aLink.attr("href"));
            links.add(newLink);
        }
        linkCount.put("external", numExternal);
        linkCount.put("intra-page", numInternalPage);
        linkCount.put("intra-site", numInternal);
        
        extractMedia("audio", currentPage);
        extractMedia("video", currentPage);
        extractMedia("other", currentPage);
    }
    
    /**
     * Extract media tags from the HTML and sort them into the appropriate container.
     * 
     * @param theType is the type of media to be extracted, e.g. video, audio, etc.
     */
    public void extractMedia(String theType, Document currentPage)
    {
    	Elements extractedMedia;
    	if(theType == "other") // If looking for non-categorized files
    		extractedMedia = currentPage.select("link[rel='alternate']");
    	else
    		extractedMedia = currentPage.getElementsByTag(theType);
        for (Element aMedia : extractedMedia)
        {
        	// File size detection not yet implemented
        	// Set to 5 as default
        	double newFileSize = 5;
        	
        	Media newMedia = new Media(newFileSize, aMedia.attr("src"));
        	
        	if(theType == "audio")
        		videos.add(newMedia);
        	else if(theType == "video")
        		audio.add(newMedia);
        	else
        		other.add(newMedia);
        }
    }
    
    /**
     * Classify images as internal or external.
     * 
     * Returns true for internal, false for external.
     * 
     * @param imageURI source to the image.
     */
    public boolean classifyImage(String imageURI)
    {
    	return !(imageURI.contains("http"));
    }
    
    /**
     * Classify links as inter-page, intra-page, or external
     * 
     * @param linkURL source from anchor tag
     * 
     * returns classification as a string
     */
    public String classifyLink(String linkURL)
    {
    	// To be implemented later
    	if(linkURL.contains("http"))
    		return "external";
    	else if(linkURL.contains("#"))
    		return "intra-page";
    	else
    		return "intra-site";
    }
    
    /**
     * This function will check if 404 error exists on current HTML page
     * before starting the analysis.
     * 
     * Boolean value false is returned if page has a 404 error.
     * 
     * Boolean value true is returned otherwise.
     * 
     * Analysis of that page is skipped if false is returned.
     * 
     * @param page is the file to be checked for 404 error before being analyzed.
     */
    public boolean notError404Checker(File page) throws FileNotFoundException
    {
    	Scanner myScan = new Scanner(page);
    	String html = myScan.useDelimiter("\\A").next();
    	myScan.close();
    	return !(html.contains("404")) ;
    }
    
    
    /**
     * The purpose of this function is to translate links found to a relative local path 
     * before adding it to the links list.
     * 
     * If translated path has already been added to the the links list, it is not going to be added again.
     * 
     * @param file is the file thats path will be taken for translation.
     */
    public String translateLinks(File file) throws IOException
    {
    	String path = file.getCanonicalPath();
    	return path.substring(rootPath.length());
    	
    }
    
    public String getFileSize()
    {
    	long fileSize = 0;
    	for (Image image : images) { fileSize += image.getFileSize(); }
    	for (Media video : videos) { fileSize += video.getFileSize(); }
    	for (Media sounds : audio) { fileSize += sounds.getFileSize(); }
    	for (Media misc : other) { fileSize += misc.getFileSize(); }
    	String fileSizeSimple;
    	//break down the number until it fits a kB or mB size
    	int magnitude = 0;
    	while (fileSize > 1024)
    	{
    		fileSize /= 1024;
    		magnitude++;
    	}
    	switch(magnitude)
    	{
    		case 0:
    			fileSizeSimple = fileSize + "B";
    		case 1:
    			fileSizeSimple = fileSize + "kB";
    		case 2:
    			fileSizeSimple = fileSize + "mB";
    		case 3:
    			fileSizeSimple = fileSize + "gB";
    		default:
    			fileSizeSimple = fileSize + "something went wrong here";
    	}
    	return fileSizeSimple;
    }
    
    /*
     * In order to assess what pages an image gets used on, it's important that we
     * be able to ascertain each image's absolute path, in order to distinguish them.
     * 
     * @param imagePath_ is the path of the image
     */
    public String getImageAbsPath(String imagePath_)
    {
    	String imagePath = imagePath_; //relative path of image
    	if (imagePath.startsWith("http") || imagePath.startsWith("www"))
    	{
    		return imagePath;
    	}
    	//substring file name from filePath
    	int cut = path.lastIndexOf("/"); //
    	String cutFilePath = path.substring(0, cut+1);
    	String cutImagePath = "";
    	//for each .., substring one level of directory from imagePath
    	if (imagePath.charAt(0) == '.') { cutImagePath = imagePath.substring(2); }
    	while (true)
    	{
    		if (cutImagePath.substring(0,2) == "..")
    		{
    			cut = cutFilePath.lastIndexOf("/");
    			//this code below brings us up the directory hierarchy
    			//for each .. we see in the image's relative path
    			cutFilePath = cutFilePath.substring(0, cut+1);
    			cutImagePath = cutImagePath.substring(3);
    		}
    		else{ break;}
    	}
    	//finally, concat filePath and imagePath, that should form a proper absolute path for the image
    	String absoluteImagePath = cutFilePath + cutImagePath;
    	return absoluteImagePath;
    }
}
